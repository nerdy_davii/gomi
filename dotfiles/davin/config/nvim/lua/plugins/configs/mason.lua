return {
  ensure_installed = {
    "gofumpt",
    "goimports",
    "golines",
    "gopls",
    "lua-language-server",
    "stylua",
    "pyright",
    "mypy",
    "debugpy",
    "clangd",
  },
}
