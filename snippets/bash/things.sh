#!/bin/bash

for ARGUMENT in "$@"
do
    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    VALUE=$(echo $ARGUMENT | cut -f2 -d=)
    case "$KEY" in
            env)    ENVIRONMENT=${VALUE} ;;
            *)
    esac
    case "$KEY" in
            ver)    BUILDVERSION=${VALUE} ;;
            *)
    esac
done
if [[ $ENVIRONMENT != "dev" && $ENVIRONMENT != "sit" && $ENVIRONMENT != "preprod" && $ENVIRONMENT != "prod" ]]; then
    echo "Environment must be specified. e.g. env=dev"
    exit
fi


